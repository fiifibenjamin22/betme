const express = require('express');
const cors = require('cors');
const body_parser = require('body-parser');
const cookie_parser = require('cookie-parser');
const morgan = require('morgan');
const mongoose = require('mongoose')
const mongodbUri = require('mongodb-uri');
var mysql = require('mysql');
const app = express();

// Files
var config = require('./Configs/Configs');

// connect to sql
// var con = mysql.createConnection({

// });

// con.connect((err) => {
// 	if (err) throw err.message;
// 	console.log("connected");
// })

// connect to mongodb
mongoose.Promise = global.Promise
var uri = config.liveConnection;
var mongooseConnectString = mongodbUri.formatMongoose(uri);
mongoose.connect(mongooseConnectString,{
	useNewUrlParser: true,
	reconnectInterval: Number.MAX_VALUE,
	reconnectInterval: 1000
 });

// Test for connection success
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error: '));
db.once('open', function callback () {
    console.log('Successfully connected to MongoDB');
});

// allow cross origin on a different port
app.all("*", function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header(
		"Access-Control-Allow-Headers",
		"Origin, X-Requested-With, Content-Type, Accept"
	);
	next();
});

app.use(cors());
app.use(body_parser.json())
app.use(body_parser.urlencoded({
    extended: false
}));
app.use(express.json());
app.use(express.urlencoded({
	extended: false
}));
app.use(cookie_parser());
app.use(morgan('dev'));

//handle keep alive
(function runForever(){
	console.log("Welcome OCTOPUS: 🐙  👉🏽 keeping server alive")
	setTimeout(runForever, 1000)
  })()

//error handling
app.use((err, req, res, next) => {
    res.status(400).send({
        error: err.message
    });
});

var server = app.listen(process.env.PORT || 3000, () => {
	var port = server.address().port;
	console.log(`Server is runing on port: ${port}`);
});
